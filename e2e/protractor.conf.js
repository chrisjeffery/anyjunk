exports.config = {
  specs: ['*.js'],
  capabilities: {
    browserName: 'chrome'
  },
  seleniumAddress: 'http://localhost:4444/wd/hub',
  framework: 'mocha',
  baseUrl: 'http://localhost:3000',
  onPrepare: function() {
    browser.ignoreSynchronization = true;
  },
  /*plugins: [{
      package: 'protractor-notify-plugin',
      notifier: 'notifysend'
  }],*/
};

