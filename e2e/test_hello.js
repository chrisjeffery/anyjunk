var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect  = chai.expect;


beforeEach(function() {
    browser.get('/');
})

describe('A venue choosing app', function() {

  it('should display a list of venues', function() {
    var list_elem = element(by.tagName('ul'));
    var venue_elements = list_elem.all(by.css('.venue'));
    expect(venue_elements.count()).to.eventually.equal(4);
  });
});

