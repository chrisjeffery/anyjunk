"use strict";
import React from 'react';
import VenueList from './VenueList.jsx';


/*can possibly make these faster with ES6 set operations */
const personCanEatAtVenue = (person, venue) =>
  venue.food.some( food => (!person.wont_eat.includes(food)));

const personCanDrinkAtVenue = (person, venue) =>
  venue.drinks.some( drink => person.drinks.includes(drink));

const venueSufficientForPeople = (venue, people) =>
  people.every( person => personCanEatAtVenue(person, venue) && personCanDrinkAtVenue(person, venue));



/* pure, stateful, container component */
class VenueOptions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {'venues': [], 'people': []}
  }

  componentDidMount() {
    fetch('/data/data.json', {
      method: 'get'
    })
    .then(response => response.json())
    .then(this.setState.bind(this))
    .catch(err => console.log('failed to get data...'));
  }

  componentWillUnmount() {
    this.serverRequest.abort();
  }

  render() {
    let potentialVenueNames = this.state.venues.filter( 
        venue => venueSufficientForPeople(venue, this.state.people) 
    ).map(venue => venue.name);

    return (
      <VenueList venueNames={potentialVenueNames} />
    );
  }
}



export default VenueOptions;
