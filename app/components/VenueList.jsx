import React from 'react'; 

/* React is used by jsx after transformation
 *
 * alternatively, could write this as a factory function
 * and inject React from the container function */

export default props => {
  return (
    <ul>
      {props.venueNames.map( 
          venueName => ( <li key={venueName}>{venueName}</li>)
      )}
    </ul>
  )
}

