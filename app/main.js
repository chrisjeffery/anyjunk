"use strict";
const React = require('react');
const ReactDOM = require('react-dom');


import VenueOptions from './components/VenueOptions.jsx';

ReactDOM.render(
  <VenueOptions />,
  document.getElementById('react-app-main')
);
