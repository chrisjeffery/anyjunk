import jsdom from 'mocha-jsdom';
import chai from 'chai';
const expect = chai.expect;

import React from 'react';
import ReactDOM from 'react-dom';

import TestUtils from 'react-addons-test-utils';

import fetchMock from 'fetch-mock';

import VenueOptions from '../app/components/VenueOptions.jsx';


const test_venues = [
{
  "name": "El Cantina",
  "food": ["Mexican"],
  "drinks": ["Soft drinks", "Tequila", "Beer"]
},
{
  "name": "Twin Dynasty",
  "food": ["Chinese"],
  "drinks": ["Soft Drinks", "Rum", "Beer", "Whisky", "Cider"]
},
{
  "name": "The World's End",
  "food": ["Eggs", "Meat", "Fish", "Pasta", "Dairy"],
  "drinks": ["Vokda", "Gin", "whisky", "Rum", "Cider", "Beer", "Soft drinks"]
}
]

const test_people = [
{
  "name": "Bobby Robson",
  "wont_eat": ["Mexican"],
  "drinks": ["Vokda", "Gin", "whisky", "Rum", "Cider", "Beer", "Soft drinks"]
},
{
  "name": "David Lang",
  "wont_eat": ["Chinese"],
  "drinks": ["Beer", "cider", "Rum"]
} 
]


/* test currently broken as the component is directly making ajax request
 * should extract the json fetch into another module, say 'api'
 * and pass into into the top level component as a prop
 * this way we can inject a mock for the api interface
 */

describe('a venue display component', () => {
  let reactContainer;

  jsdom()

  afterEach('unmount component and reset document', () => {
    ReactDOM.unmountComponentAtNode(reactContainer);
    reactContainer = null;
    document.body.innerHTML = "";
  })


  it('renders two names', () => {
    reactContainer = document.createElement('div');


    let renderedComponent = ReactDOM.render(
       <VenueOptions venues={test_venues} people={test_people} />,
       reactContainer
    );

    let listItem = TestUtiils.findRenderedDOMComponentWithTag(
      reactContainer,
      'li'
    );

    expect(listItem.textContent).to.equal('blah')

  });

})
