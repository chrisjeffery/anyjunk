const gulp = require('gulp');

const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');


const browserify = require('browserify');
const watchify = require('watchify');
const babelify = require('babelify');

const sourcemaps = require('gulp-sourcemaps');

const gutil = require('gulp-util');
const notify = require('gulp-notify');

const protractor = require('gulp-protractor').protractor;
//const webdriver_standalone = require('gulp-protractor').webdriver_standalone;
//const webdriver_update = require('gulp-protractor').webdriver_update;


//TODO add sourcemaps


const bundler = watchify(browserify({
  entries: ['./app/main.js'],
  transform: [babelify],
  debug: true,
  cache: {}, //for watchify
  packageCache: {}, //for watchify
  //fullPaths: true //DEV for sourcemaps.
}));


function bundle() {
  return bundler.bundle()
    .on('error', function(error) {
      notify.onError({
        title: error.description,
        message: ('Line: ' +  error.lineNumber + '\n' +
          'Column: ' + error.column  + '\n\n' + error.message),
        icon: 'dialog-error'
      })(error);
      gutil.log.bind(gutil, 'browserify error')(error);
    })
    //.on('error', gutil.log.bind(gutil, 'browserify error'))
    /*.on('error', )*/
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(gulp.dest('./build/'));
};


//gulp.task('webdriver_standalone', webdriver_standalone);

gulp.task('e2e', function() {
  gulp.src('./e2e/*.js')
    .pipe(protractor({
      configFile: './e2e/protractor.conf.js'
    }))
    .on('error', function(error) {
      notify({
        title: error.description,
        message: ('Line: ' +  error.lineNumber + '\n' +
                  'Column: ' + error.column  + '\n\n' + 
                  error.message),
        icon: 'dialog-error'
      })
    });
});

gulp.task('watchtests', function() {
  gulp.watch(['./build/*.js', './e2e/*.js'], ['e2e']);
});


gulp.task('js', bundle);
bundler.on('update', bundle);
bundler.on('log', gutil.log);

gulp.task('default', ['js']);
