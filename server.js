const express = require('express');
const app = express();

app.use(express.static(__dirname + '/build'));

app.set('view engine', 'ejs')

app.get('/', (req, res) => {
  console.log('render request');
  res.render('index');
})

app.listen(3000, '0.0.0.0', () => console.log('listening on port 3000'));
